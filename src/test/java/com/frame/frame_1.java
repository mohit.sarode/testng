package com.frame;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class frame_1 {
	
	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver=  new ChromeDriver();
		driver.get("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");   
		driver.manage().window().maximize();
 
		driver.switchTo().frame("packageListFrame");
		
		Thread.sleep(2000);
		//driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);	
		driver.findElement(By.partialLinkText("org.openqa.selenium")).click();
		Thread.sleep(2000);
		driver.navigate().refresh();
		
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		
		Thread.sleep(2000);
		driver.switchTo().frame("packageFrame");
		driver.findElement(By.partialLinkText("Alert")).click();
		
		
	}
	
	
	

}
